<?php
namespace P8;

error_reporting(false); # @todo   base this on dev/prod env

use \Exception;
use P8\Middleware\Authenticator,
    P8\Middleware\Request,
    P8\Middleware\Router,
    P8\Middleware\Terminator;

class Pipeline {

    /** */
    private $in, $out;
    
    /** */
    public function __construct(
        private ?array $middleware = [],
        private mixed  $settings   = null
    )
    {
        $this->loadDefaultMiddleware();
    }
    
    /**
     * [ ? ]
     *
     * @todo   provide a "hook" mechanism which app developers
     *         could use to inject their own middleware
     */
    private function loadDefaultMiddleware() : void
    {
        if (!empty($this->middleware)) {
            return;
        }

        $this->middleware = [ new Request,
                              new Authenticator,
                              new Router ];
    }

    /**
     * [ ? ]
     *
     * @param  ?int $i
     *
     * @return mixed
     */
    public function execute(?int $i = 0) : mixed
    {
        try {
            return isset($this->middleware[$i])
                ? $this->middleware[$i]->run(
                    $this->in,
                    $this->out,
                    function () use ($i) { return $this->execute($i+1); }
                ) : (new Terminator)->run($this->in, $this->out, null);
        } catch (Exception $e) {
            $this->out->setError($e->getMessage());
            return $this->out;
        }
    }
}

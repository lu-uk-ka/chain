<?php
namespace P8\Gears\Http;

class Request {

    private $cookies,
        $headers,
        $origin,
        $parameters,
        $uri,
        $verb;
    
    public function __construct()
    {
        $this->cookies          = $_COOKIE;
        $this->headers          = getallheaders();
        $this->origin           = $_SERVER['REMOTE_ADDR'];
        $this->verb             = $_SERVER['REQUEST_METHOD'];

        @[ $this->uri, $query ] = explode('?', $_SERVER['REQUEST_URI']);
        parse_str($query, $this->parameters); 
    }

    /** */
    private function getCookie(string $key) : mixed
    {
        return isset($this->cookies[$key])
            ? $this->cookies[$key] : null;
    }

    /** */
    public function getHeader(string $key) : mixed
    {
        return isset($this->headers[$key])
            ? $this->headers[$key] : null;
    }
    
    /**
     * @todo   DRY - retrieve cookie/headers/parameters magically
     */
    public function getParameter(string $key) : mixed
    {
        return isset($this->parameters[$key])
            ? $this->parameters[$key] : null;
    }

    /** */
    public function getParameters() : ?array
    {
        return $this->parameters;
    }

    /**
     * [ ? ]
     *
     * @return ?string
     * @todo   de-hardcode token name, load a dotenv
     */
    public function getToken() : ?string
    {
        return $this->getCookie('SESSI');
    }

    /** */
    public function getUri() : ?string
    {
        return $this->uri;
    }

    /** */
    public function getVerb() : ?string
    {
        return $this->verb;
    }
}

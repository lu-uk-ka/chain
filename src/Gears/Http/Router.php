<?php
namespace P8\Gears\Http;

use Exception,
    ReflectionClass;

use P8\Gears\DependencyInjection\Container as Registry;

class Router {

    /** */
    public function __construct(private string $uri)
    {
        $this->registry = new Registry;
    }

    /**
     * [ ? ]
     *
     * @todo   any non-matched route within a resource gets matched as an ID
     *         ie /api/statistics/reports/abc/d will send 'abc'
     *         as an ID to Reports::read
     */
    public function resolve(Request $in) : mixed
    {
        @[
            $group,
            $domain,
            $resource,
            $id
        ] = $segments = array_values(
            array: array_filter(
                array:explode(
                    '/',
                    $in->getUri()
                )
            )
        );

        $ctl       = sprintf(
            'App\\Domain\\%s\\Controller\\%s',
            ucfirst($domain),
            ucfirst($resource)
        );

        if (! class_exists($ctl)) {
            throw new Exception(sprintf('Controller %s not found', $ctl), 404);
        }

        $ref       = new ReflectionClass($ctl);
        $action    = $this->getActionFromAnnotation($ref, $id)
                   ?: $this->getActionFromVerb($in->getVerb(), isset($id));

        if (! $action) {
            throw new Exception('No route found', 404);
        }

        $class  = $this->registry->resolve($ref);
        if (! method_exists($class, $action)) {
            throw new Exception('Route action undefined', 404);
        }
        
        return $class->$action($in, $id);
    }

    /** */
    private function getActionFromAnnotation(ReflectionClass $ref, ?string $target) : ?string
    {
        foreach ($ref->getMethods() as $method) {
            foreach ($method->getAttributes(RouteDemo::class) as $route) {
                if ($route->newInstance()->match($target)) {
                    return $method->getName();
                }
            }
        }

        return null;
    }

    /** */
    private function getActionFromVerb(string $verb, bool $identified = false) : ?string
    {
        return match($verb) {
            'POST'   => 'create',
            'GET'    => $identified ? 'read' : 'list',
            'PUT'    => 'update',
            'DELETE' => 'delete',
            'PATCH'  => 'change',
            default  => null         
        };
    }
}

<?php
namespace P8\Gears\Http;

use Attribute;

#[Attribute]
class RouteDemo {

    /** */
    public function __construct(private string $uri)
    {
    }

    /** */
    public function match(?string $uri) : bool
    {
        if (!$uri) {
            return false;
        }
        return (bool) preg_match('/' . preg_quote($uri) . '/', $this->uri);
    }

}

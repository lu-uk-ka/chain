<?php
namespace P8\Gears\Http;

use Exception;

class Response {

    /** */
    private ?array $body;

    /** */
    public function __construct(private $accept = 'application/json')
    {
        $this->body = [
            'data'    => '',
            'error'   => '',
            'message' => '',
        ];
    }
   
    /**
     * [ ? ]
     *
     * @return string
     *
     * @throws Exception
     */
    public function __toString() : string
    {
        if (!isset($this->body)) {
            throw new Exception('No response', 418);
        }

        $this->body['error'] = strlen($this->body['message']) > 0;
        return $this->negotiateContentAtType();
    }

    /** */
    private function negotiateContentAtType() : mixed
    {
        return match($this->accept) {
            'application/json' => json_encode($this->body),
            'application/xml'  => '<some>xml</some>', # @todo    ?
            'text/html'        => $this->body['data'],
            default            => json_encode($this->body)
        };
    }

    /** */
    public function setBody(mixed $content) : void
    {
        $this->body['data']  = $content;
    }

    public function setError(?string $content) : void
    {
        $this->body['message'] = $content;
    }
}

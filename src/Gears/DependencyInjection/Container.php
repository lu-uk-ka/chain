<?php
namespace P8\Gears\DependencyInjection;

use ReflectionClass;

class Container {

    /**
     * @todo   test registry integrity
     */
    private array $registry = [];

    /** */
    public function resolve(ReflectionClass $ref) : object
    { 
        if (!isset($this->registry[$ref->getName()])) {
            $this->registry[$ref->getName()] = $this->inject($ref);
        }
        
        return $this->registry[$ref->getName()];
    }

    /** */
    private function inject(ReflectionClass $ref)
    {
        $init       = $ref->getConstructor();
        if (!$init) {
            return $ref->newInstance();
        }

        $injections = $init->getParameters();
        if (!$injections) {
            return $ref->newInstance();
        }

        $injections = array_map(
            array: $injections,
            callback: fn ($i) => $this->resolve(new ReflectionClass($i->getType()->getName()))
        );

        return $ref->newInstanceArgs($injections);
    }
}

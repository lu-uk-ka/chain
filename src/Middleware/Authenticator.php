<?php
namespace P8\Middleware;

# @todo   make custom exceptions, avoid re-importing this everywhere
use Exception;

class Authenticator implements MiddlewareInterface {

    /**
     * [ ? ]
     *
     * @todo   an auth server should provide identity, session,
     *         default to something for time being
     */
    public function run(&$in, &$out, $next) : mixed
    {
        if (!$in->getToken()) {
            $demo = 'Will default to a SESSI=anything-here cookie in subsequent requests for demo purposes.';
            setCookie('SESSI', 'X');
            throw new Exception('Request stalled, identity missing. ' . $demo, 401);
        }

        return $next();
    }

}

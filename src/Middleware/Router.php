<?php
namespace P8\Middleware;

use Exception;
use P8\Gears\Http\Router as RouteResolver;

class Router implements MiddlewareInterface {

    public function __construct(private ?array $routes = [])
    {
    }

    /** */
    public function run(&$in, &$out, $next) : mixed
    {
        $resolver = new RouteResolver($in->getUri());
        $out->setBody($resolver->resolve($in));

        return $next();       
    }
}

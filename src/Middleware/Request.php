<?php
namespace P8\Middleware;

use P8\Gears\Http\Request as HttpRequest,
    P8\Gears\Http\Response as HttpResponse;

class Request implements MiddlewareInterface {

    /** */
    public function run(&$in, &$out, $next) : mixed
    {
        $in  = new HttpRequest;
        $out = new HttpResponse(accept: $in->getHeader('Accept'));

        return $next();
    }

}

<?php
namespace P8\Middleware;

interface MiddlewareInterface {

    /**
     * [ ? ]
     *
     * @todo   rethink taking in references
     *
     * @return mixed
     */
    public function run(&$in, &$out, $next) : mixed;

}

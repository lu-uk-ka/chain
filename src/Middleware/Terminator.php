<?php
namespace P8\Middleware;

use P8\Pipeline;

class Terminator implements MiddlewareInterface {

    public function run(&$in, &$out, $next) : mixed
    {
        return $out;
    }
    
}

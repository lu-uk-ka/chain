<?php
namespace P8;

spl_autoload_register(function ($c) {
    include str_replace([ '\\', 'P8', 'App' ], [ '/', 'src', 'app' ], $c) . '.php';
});

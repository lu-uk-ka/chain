<?php
namespace App\Domain\Default\Repository;

interface RepositoryInterface {

    /** */
    public function findBy(array $map) : mixed;

}

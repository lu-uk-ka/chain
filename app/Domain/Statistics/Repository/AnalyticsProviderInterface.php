<?php
namespace App\Domain\Statistics\Repository;

interface AnalyticsProviderInterface {

    public function getName() : string;
    
}

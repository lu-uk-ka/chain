<?php
namespace App\Domain\Statistics\Repository;

use App\Domain\Default\Repository\RepositoryInterface;

class OtherAnalytics implements RepositoryInterface, AnalyticsProviderInterface {

    /** */
    public function getName() : string
    {
        return 'Another provider';
    }
    
    /** */
    public function findBy(array $map) : mixed
    {
        return 321;
    }

}

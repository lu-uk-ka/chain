<?php
namespace App\Domain\Statistics\Repository;

use App\Domain\Default\Repository\RepositoryInterface,
    App\Domain\Default\Service\Helper\cURL;

/**
 * [ ? ]
 *
 * @todo   composer require google/apiclient
 * @todo   provide auth secrets, use cURL Helper
 */
class GoogleAnalytics implements RepositoryInterface, AnalyticsProviderInterface {

    /** */
    public function getName() : string
    {
        return 'Google Analytics';
    }
    
    /**
     * [ ? ]
     *
     * @todo   use $map for date range querying
     */
    public function findBy(array $map) : mixed
    {
        if (! $raw = file_get_contents('http://web/static/ga.json')) {
            return null;
        }
        if (! $res = json_decode($raw)) {
            return null;
        }

        return $res->pageviews;
    }

}

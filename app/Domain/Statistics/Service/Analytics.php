<?php
namespace App\Domain\Statistics\Service;

use App\Domain\Statistics\Repository\{ Activities, GoogleAnalytics, OtherAnalytics };

class Analytics {

    /** */
    public function __construct(
        private GoogleAnalytics $ga,
        private OtherAnalytics  $other,
        private Activities      $internal,
    )
    {
    }
    
    /** */
    public function setRange(?int $start, ?int $end) : void
    {
        $this->params = [
            'start' => $start ?? 0,
            'end'   => $end   ?? 0
        ];
    }
    
    /**
     * @todo   use ranges 
     */
    public function getReports()
    {
        return [ $this->ga->getName()    => $this->ga->findBy($this->params),
                 'database'              => $this->internal->findBy($this->params),
                 $this->other->getName() => $this->other->findBy($this->params) ];
    }

}

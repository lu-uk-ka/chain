<?php
namespace App\Domain\Statistics\Controller;

use P8\Gears\Http\Request,
    P8\Gears\Http\RouteDemo;

use App\Domain\Statistics\Service\Analytics;

class Reports /* extends Controller */ {

    /** */
    public function __construct(
        private Analytics $analytics
    )
    {
        # parent::__construct();
    }
    
    /** */
    public function list(Request $req)
    {
        $this->analytics->setRange(
            $req->getParameter('start'),
            $req->getParameter('end')
        );

        return $this->analytics->getReports();
    }

    /** */
    public function read(Request $req, $id)
    {
        return [ 'data' => [ 'id' => $id ] ];
    }

    /** */
    #[RouteDemo('/calculate-totals')]
    public function getTotals(Request $req)
    {
        return 1+2+3;
    }

}

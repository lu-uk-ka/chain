# P8 experiment

a (+non)RESTful API nanoframework built as to experiment with PHP8.

## Contents

 - [Installation](#Installation)
 - [Usage](#Usage)

## Installation

Prerequisites:
    - docker-compose

To install and run, execute

```
make up
```

## Usage

Navigate a browser to http://localhost/api/statistics/reports?start=0&end=5
or to http://localhost/api/statistics/reports/calculate-totals
